# Maquette Front Kinomap

L'objectif est de reproduire cette maquette avec le plus de fidélité possible.
<br/>
Le header et le footer ne sont pas a réaliser.

## **Stack imposé : Angular, Typescript & Scss**

liens du Figma : https://www.figma.com/file/cFRPc2FLS5ujrsIPCVXFYR/FrontProject?node-id=0%3A1&t=3gVKKp896LMYZBOq-0

### API : http://preprod.kinomap.com:3333/

**endPoints:**
- /score : Get application score value
```json
return number
```

- /subscriptions : Get the list of Subscriptions (always 3 versions)
```json
return {
    name: string,
    price: number,
    period: number (1 | 12 | 99),
    users: number,
    best_value?: boolean
}[]
```

- /faq : Get the list of FAQ 
```json
return {title: string, details: string}[]
```

- /free-period : Get the free period
```json
return number
```